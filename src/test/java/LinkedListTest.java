import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


public class LinkedListTest {

    @Test public void testIsEmpty() {
        LinkedList list = new LinkedList();
        assertTrue(list.isEmpty());
    }

    @Test public void testLength() {
        LinkedList list = new LinkedList();
        assertEquals(list.length(), 0);
    }

    @Test public void testAddToHead() {
        LinkedList list = new LinkedList();
        list.addToHead(3);
        assertEquals(list.length(), 1);
        assertFalse(list.isEmpty());
    }

    @Test public void testDeleteNode() {
        LinkedList list = new LinkedList();
        list.addToHead(3);
        list.addToHead(42);

        list.deleteNode(2);
        assertEquals(list.length(),1);
        assertFalse(list.isEmpty());
    }

    @Test public void testDeleteNodeWhenNotExist() {
        LinkedList list = new LinkedList();
        list.addToHead(3);

        list.deleteNode(333);
        assertEquals(list.length(),1);
        assertFalse(list.isEmpty());
    }

    @Test public void testDeleteNodeWhenLastNode() {
        LinkedList list = new LinkedList();
        list.addToHead(3);

        list.deleteNode(1);

        assertEquals(list.length(),0);
        assertTrue(list.isEmpty());
    }

    @Test public void testDisplayNode() {
        LinkedList list = new LinkedList();
        list.addToHead(3);

        String data = list.displayNode(1);

        assertEquals(data, "3");
    }

    @Test public void testDisplayNodeWhenNotExist() {
        LinkedList list = new LinkedList();
        list.addToHead(3);

        String data = list.displayNode(42);

        assertEquals(data, "");
    }
}
