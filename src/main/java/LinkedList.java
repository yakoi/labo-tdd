import java.util.List;


public class LinkedList {

    private class Node {

        public int data;
        public Node next;

        public Node(int d, Node n) {
            data = d;
            next = n;
        }

    }

    private Node head;

    public void addToHead(int d) {
        head = new Node(d, head);
    }

    public void deleteNode(int index){
        Node node = head;
        Node previous_node = null;
        int current = 1;

        if (index == 1) {
            head = node.next;
            node = null;
        }

        while (node != null) {

            if (index == current) {
                assert current != 1;
                assert previous_node != null;
                previous_node.next = node.next;
            }

            previous_node = node;
            node = node.next;
            current++;
        }

    }

    public String displayNode(int index){
        Node node = head;
        int current = 1;

        while (node != null) {
            if (index == current) {
                return Integer.toString(node.data);
            }

            node = node.next;
            current++;
        }
        return "";
    }

    public boolean isEmpty() {
        return (head == null);
    }

    public int length() {
        int l = 0;
        Node n = head;

        while (n != null) {
            l++;
            n = n.next;
        }

        return l;
    }
}
